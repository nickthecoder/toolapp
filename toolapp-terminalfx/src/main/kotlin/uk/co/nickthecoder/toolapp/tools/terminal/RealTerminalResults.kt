/*
ParaTask Copyright (C) 2017  Nick Robinson>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.toolapp.tools.terminal

import javafx.application.Platform
import javafx.scene.input.DataFormat
import javafx.scene.input.MouseButton
import javafx.scene.input.MouseEvent
import javafx.scene.input.TransferMode
import uk.co.nickthecoder.paratask.gui.CompoundDropHelper
import uk.co.nickthecoder.paratask.gui.DropFiles
import uk.co.nickthecoder.paratask.gui.SimpleDropHelper
import uk.co.nickthecoder.paratask.util.process.OSCommand
import uk.co.nickthecoder.toolapp.Tool
import uk.co.nickthecoder.toolapp.ToolApp
import uk.co.nickthecoder.toolapp.project.AbstractResults
import java.nio.charset.Charset
import com.kodedu.terminalfx.*
import com.pty4j.PtyProcess
import javafx.scene.control.TabPane

class RealTerminalResults(tool: Tool)

    : AbstractResults(tool, "Terminal"), TerminalResults {

    override val node = TabPane().apply {
        val terminalBuilder = TerminalBuilder()
        tabs.add( terminalBuilder.newTerminal() )
    }

    override var process: PtyProcess? = null

    //var session: TerminalSession? = null

    var triedStopping: Boolean = false

    val textDropHelper = SimpleDropHelper<String>(DataFormat.PLAIN_TEXT, arrayOf(TransferMode.COPY)) { _, text ->
        // TODO
        // sendText(text)
    }
    val filesDropHelper = DropFiles(arrayOf(TransferMode.COPY)) { _, files ->
        val text = files.joinToString(separator = " ") { quoteFilenameIfNeeded(it.path) }
        // TODO
        // sendText(text)
    }
    val compoundDropHelper = CompoundDropHelper(filesDropHelper, textDropHelper)

    init {
        node.addEventHandler(MouseEvent.MOUSE_CLICKED) { event ->
            if (event.clickCount == 1 && event.button == MouseButton.PRIMARY) {
                focus()
            }
        }
    }

    fun quoteFilenameIfNeeded(filename: String): String {
        if (filename.matches(Regex("[a-zA-Z0-9,._+:@%/-]*"))) {
            return filename
        }
        return "'" + filename.replace("//", "////").replace("'", "'\\''") + "'"
    }

    override fun start(osCommand: OSCommand) {
        triedStopping = false
    }

    fun copyEnv(): MutableMap<String, String> {
        return System.getenv().toMutableMap()
    }

/*
    fun createJWidget(osCommand: OSCommand): JediTermWidget {
        val cmd = mutableListOf<String>()
        cmd.add(osCommand.program)
        cmd.addAll(osCommand.arguments)

        val env = copyEnv()
        val dir = osCommand.directory?.path

        env["TERM"] = "xterm"
        val charset = Charset.forName("UTF-8")

        val console = false

        process = PtyProcess.exec(cmd.toTypedArray(), env, dir, console)

        val connector = PtyProcessTtyConnector(process, charset)
        val settings = DefaultSettingsProvider()
        termWidget = JediTermWidget(settings)
        session = termWidget!!.createTerminalSession(connector)
        session?.start()

        compoundDropHelper.applyTo(node)

        return termWidget!!
    }
    */
/*
    fun sendText(text: String) {
        termWidget?.terminalStarter?.sendString(text)
    }
*/
    override fun waitFor(): Int {
        val exitStatus = process?.waitFor() ?: -12
        Platform.runLater {
            labelProperty.set("Finished (Exit Status=$exitStatus)")
        }
        return exitStatus
    }

    override fun detaching() {
        super.detaching()
        stop()
    }

    override fun stop() {
        if (triedStopping) {
            process?.destroyForcibly()
        } else {
            process?.destroy()
        }
        triedStopping = true
    }

    override fun focus() {
        ToolApp.logFocus("RealTerminalResults.focus. node.requestFocus()")
        node.requestFocus()
    }
}
