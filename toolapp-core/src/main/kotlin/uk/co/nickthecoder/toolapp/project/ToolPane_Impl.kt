/*
ParaTask Copyright (C) 2017  Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

package uk.co.nickthecoder.toolapp.project

import javafx.application.Platform
import javafx.event.ActionEvent
import javafx.geometry.Side
import javafx.scene.control.Button
import javafx.scene.control.SingleSelectionModel
import javafx.scene.layout.BorderPane
import uk.co.nickthecoder.toolapp.Tool
import uk.co.nickthecoder.toolapp.ToolApp
import uk.co.nickthecoder.toolapp.gui.MyTab
import uk.co.nickthecoder.toolapp.gui.MyTabPane
import uk.co.nickthecoder.toolapp.table.filter.Filtered

abstract class AbstractToolPane(override final val tool: Tool)

    : ToolPane, BorderPane() {

    final override var parametersPane: ToolParametersPane = ToolParametersPane_Impl(tool)

    override lateinit var halfTab: HalfTab

    final override val parametersTab = ParametersTab(parametersPane)

    val header: HeaderOrFooter? = tool.createHeader()

    val footer: HeaderOrFooter? = tool.createFooter()

    var filterTab: FilterTab? = null

    init {

        parametersTab.canClose = false

    }

    /**
     * Called when the MAJOR tab has changed, and is used to ensure focus on the currently selected MINOR tab.
     */
    override fun selected() {
        val tab = selectionModel.selectedItem

        if (tab is MinorTab) {
            // TODO Remove runlater. Should be using focusWhenSceneIsSet
            Platform.runLater {
                tab.focus()
            }
        }
    }

    fun onTabChanged(oldTab: MyTab?, newTab: MyTab?) {
        if (oldTab is MinorTab) {
            oldTab.deselected()
        }
        if (newTab is MinorTab) {
            newTab.selected()
        }
        if (newTab is ResultsTab && !halfTab.isDock()) {
            top = header
            bottom = footer
        } else {
            top = null
            bottom = null
        }
    }

    override fun resultsTool(): Tool {
        val tab = selectionModel.selectedItem
        return if (tab is ResultsTab) {
            tab.results.tool
        } else {
            tool
        }
    }

    protected abstract fun removeResults(results: Results): Int

    private fun removeOldResults(oldResultsList: List<Results>): Int {
        var index = 0

        // This is Order n squared, but n is small, so I won't bother optimising it!
        for (oldResults in oldResultsList) {
            val oldIndex = removeResults(oldResults)
            if (oldIndex >= 0) {
                index = oldIndex
            }
        }
        return index
    }

    override fun replaceResults(resultsList: List<Results>, oldResultsList: List<Results>) {
        removeOldResults(oldResultsList)
        parametersTab.styleClass.remove("separated")
        if (resultsList.isNotEmpty()) {
            parametersTab.styleClass.add("separated")
        }

        resultsList.forEach { results ->
            addResults(results)
        }
        // Select the first tab, unless another tab selected itself while being added.
        if (parametersTab.isSelected || filterTab?.isSelected == true) {
            selectionModel.select(0)
        }
    }

    override fun addResults(results: Results): ResultsTab {
        tabs.forEachIndexed { index, tab ->
            if (tab !is ResultsTab) {
                return addResults(results, index)
            }
        }
        return addResults(results, 0)
    }


    private var attached: Boolean = false

    override fun isAttached(): Boolean {
        return attached
    }

    override fun attached(halfTab: HalfTab) {
        this.halfTab = halfTab

        ToolApp.logAttach("ToolPane.attaching")
        parametersPane.attached(this)
        filterTab?.parametersPane?.attached(this)

        tool.attached(this)

        ToolApp.logAttach("ToolPane.attached")
        attached = true
    }

    override fun detaching() {
        attached = false
        ToolApp.logAttach("ToolPane detaching")
        parametersPane.detaching()
        filterTab?.parametersPane?.detaching()
        tool.detaching()
        parametersPane.detaching()
        removeOldResults(tool.resultsList)

        header?.detaching()

        ToolApp.logAttach("ToolPane detached")
    }

    override fun nextTab() {
        if (tabs.isNotEmpty()) {
            var index = selectionModel.selectedIndex + 1
            if (index >= tabs.size) index = 0

            selectionModel.clearAndSelect(index)
        }
    }

    override fun prevTab() {
        if (tabs.isNotEmpty()) {
            var index = selectionModel.selectedIndex - 1
            if (index < 0) index = tabs.size - 1

            selectionModel.clearAndSelect(index)
        }
    }

    override fun selectTab(index: Int) {
        if (index >= 0 && index < tabs.size) {
            selectionModel.clearAndSelect(index)
        }
    }

    override fun focusHeader() {
        if (header != null) {
            ToolApp.logFocus("ToolPane_Implt focusHeader. header.focus()")
            header.focus()
        } else {
            val results = currentResults()
            if (results is ResultsWithHeader) {
                ToolApp.logFocus("ToolPane_Implt focusHeader. results.headerRows.focus()")
                results.headerRows.focus()
            }
        }
    }

    override fun focusResults() {
        if (skipFocus) {
            println("ToolPane_Impl Skipped focus")
        } else {
            val tab = selectionModel.selectedItem
            if (tab is MinorTab) {
                ToolApp.logFocus("ToolPane_Implt focusResults. tab.focus()")
                tab.focus()
            }
        }
    }

    override var skipFocus: Boolean = false

    override fun currentResults(): Results? {
        val tab = selectionModel.selectedItem
        if (tab is ResultsTab) {
            return tab.results
        }
        return null
    }

}

class ToolPane_Impl(tool: Tool) : AbstractToolPane(tool) {

    val tabPane = MyTabPane<MinorTab>()

    override val selectionModel: SingleSelectionModel<MinorTab> = tabPane.selectionModel

    override val tabs = tabPane.tabs

    init {
        center = tabPane
        tabPane.side = Side.BOTTOM

        tabPane.add(parametersTab)
        tabPane.selectionModel.selectedItemProperty().addListener { _, oldTab, newTab -> onTabChanged(oldTab, newTab) }

        // Add the filter tab, if the tool has a filter
        if (tool is Filtered) {
            val filters = tool.rowFilters
            if (filters.isNotEmpty()) {
                filterTab = FilterTab(tool, filters)
                tabPane.add(filterTab!!)
            }
        }
    }

    override fun createAddTabButton(action: (ActionEvent) -> Unit): Button? {
        return tabPane.createAddTabButton(action)
    }

    override fun removeResults(results: Results): Int {
        for ((i, tab) in tabs.withIndex()) {
            if (tab is ResultsTab && tab.results === results) {
                tabPane.remove(tab)
                return i
            }
        }
        return -1
    }

    override fun addResults(results: Results, index: Int): ResultsTab {
        val resultsTab = ResultsTab(results)
        resultsTab.canClose = results.canClose

        tabPane.add(index, resultsTab)
        results.attached(resultsTab, this)
        return resultsTab
    }
}

