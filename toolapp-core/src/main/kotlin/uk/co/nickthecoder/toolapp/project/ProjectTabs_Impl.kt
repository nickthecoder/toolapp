/*
ParaTask Copyright (C) 2017  Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

package uk.co.nickthecoder.toolapp.project

import javafx.event.EventHandler
import javafx.geometry.Side
import javafx.scene.control.ContextMenu
import javafx.scene.control.MenuItem
import javafx.scene.image.ImageView
import javafx.scene.input.KeyEvent
import javafx.scene.input.TransferMode
import uk.co.nickthecoder.paratask.gui.DropFiles
import uk.co.nickthecoder.paratask.gui.applyDropHelper
import uk.co.nickthecoder.toolapp.TaskRegistry
import uk.co.nickthecoder.toolapp.Tool
import uk.co.nickthecoder.toolapp.ToolApp
import uk.co.nickthecoder.toolapp.gui.MyTab
import uk.co.nickthecoder.toolapp.gui.MyTabPane
import uk.co.nickthecoder.toolapp.tools.editor.EditorTool
import uk.co.nickthecoder.toolapp.tools.places.DirectoryTool

class ProjectTabs_Impl(override val projectWindow: ProjectWindow)

    : ProjectTabs, MyTabPane<MyTab>() {

    private val addTabContextMenu = ContextMenu()

    private val closedHistory = ClosedHistory()

    init {
        selectionModel.selectedItemProperty().addListener { _, oldTab, newTab ->
            onTabChanged(oldTab, newTab)
        }
        addEventHandler(KeyEvent.KEY_PRESSED) { onKeyPressed(it) }

        // Allow files to be dragged to the "+" button to view them in the text editor tool.
        createAddTabButton { onAddTab() }.applyDropHelper(DropFiles(TransferMode.ANY) { _, filesAndDirs ->
            val files = filesAndDirs.filter { it.isFile }
            val dirs = filesAndDirs.filter { it.isDirectory }
            if (files.isNotEmpty()) addTool(EditorTool(files))
            if (dirs.isNotEmpty()) addTool(DirectoryTool(dirs))
        })

        buildContextMenu()
    }

    private fun onKeyPressed(event: KeyEvent) {
        tabs.forEach { tab ->
            if (tab is ProjectTab_Impl) {
                if (tab.tabShortcut?.match(event) == true) {
                    event.consume()
                    tab.isSelected = true
                }
            }
        }
    }

    private fun onTabChanged(oldTab: MyTab?, newTab: MyTab?) {

        (oldTab as? ProjectTab_Impl)?.deselected()
        if (newTab is ProjectTab_Impl) {
            newTab.selected()
            projectWindow.toolChanged(newTab.left.toolPane.tool)
        }
    }

    override fun addTool(index: Int, tool: Tool, run: Boolean, select: Boolean): ProjectTab {
        val newProjectTab = ProjectTab_Impl(this, tool)
        add(index, newProjectTab)

        ToolApp.logAttach("ProjectTabs.attaching ProjectTab")
        newProjectTab.attached(this)
        ToolApp.logAttach("ProjectTabs.attached ProjectTab")

        if (select) {
            selectionModel.clearAndSelect(index)
        }

        if (run) {
            try {
                tool.toolPane?.parametersPane?.run()
            } catch (e: Exception) {
            }
        } else {
            tool.toolPane?.halfTab?.pushHistory()
        }
        return newProjectTab
    }

    override fun indexOf(projectTab: ProjectTab): Int {
        return tabs.indexOf(projectTab as MyTab)
    }

    override fun addTool(tool: Tool, run: Boolean, select: Boolean): ProjectTab {
        return addTool(tabs.size, tool, run = run, select = select)
    }


    override fun addAfter(after: ProjectTab, tool: Tool, run: Boolean, select: Boolean): ProjectTab {
        val index = indexOf(after)
        return addTool(index + 1, tool, run = run, select = select)
    }

    override fun currentTab(): ProjectTab? {
        return selectionModel.selectedItem as? ProjectTab
    }

    override fun removeTab(projectTab: ProjectTab) {
        remove(projectTab as MyTab)
    }

    override fun remove(tab: MyTab) {
        val projectTab = (tab as ProjectTab)
        closedHistory.remember(projectTab)
        projectTab.detaching()
        super.remove(tab)
    }

    override fun restoreTab() {
        if (closedHistory.canRestore()) {
            closedHistory.restore(this)
        }
    }

    override fun split() {
        currentTab()?.split()
    }

    override fun splitToggle() {
        currentTab()?.splitToggle()
    }

    override fun duplicateTab() {
        currentTab()?.duplicateTab()
    }

    override fun listTabs(): List<ProjectTab> {
        return tabs.filterIsInstance<ProjectTab>()
    }

    override fun nextTab() {
        if (tabs.isNotEmpty()) {
            var index = selectionModel.selectedIndex + 1
            if (index >= tabs.size) index = 0

            selectionModel.clearAndSelect(index)
        }
    }

    override fun prevTab() {
        if (tabs.isNotEmpty()) {
            var index = selectionModel.selectedIndex - 1
            if (index < 0) index = tabs.size - 1

            selectionModel.clearAndSelect(index)
        }
    }

    override fun selectTab(index: Int) {
        if (index >= 0 && index < tabs.size) {
            selectionModel.clearAndSelect(index)
        }
    }

    override fun selectTab(projectTab: ProjectTab) {
        selectedTab = projectTab as ProjectTab_Impl
    }

    private fun buildContextMenu() {

        TaskRegistry.home.listTasks().filterIsInstance<Tool>().forEach { tool ->
            val imageView = tool.icon?.let { ImageView(it) }
            val item = MenuItem(tool.shortTitle, imageView)

            item.onAction = EventHandler {
                addTool(tool.copy())
            }
            addTabContextMenu.items.add(item)
        }
    }

    fun onAddTab() {
        addTabContextMenu.show(extraControl, Side.BOTTOM, 0.0, 0.0)
    }

}
