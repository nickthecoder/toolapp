/*
ParaTask Copyright (C) 2017  Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

package uk.co.nickthecoder.toolapp.table

import javafx.beans.value.ObservableValue
import javafx.scene.control.TableColumn
import uk.co.nickthecoder.paratask.util.Labelled
import uk.co.nickthecoder.paratask.util.uncamel

open class Column<R, T>(
        val name: String,
        width: Int? = null,
        override val label: String = name.uncamel(),
        val getter: (R) -> T,
        val filterGetter: (R) -> Any? = getter)

    : TableColumn<WrappedRow<R>, T>(label), Labelled {

    /**
     * Set this to true for all columns, which together form a unique identifier for the row.
     * For the DirectoryTool, this is the filename column. For the OptionsTool, it is the code column.
     * [identifier] is a handy fluent API way of setting this to true.
     */
    var isIdentifier: Boolean = false

    init {
        @Suppress("UNCHECKED_CAST")
        setCellValueFactory { p -> p.value.observable(name, getter) as ObservableValue<T> }
        isEditable = false
        if (width != null) {
            prefWidth = width.toDouble()
        }
    }

    /**
     * A fluent API for setting [isIdentifier] to true.
     */
    fun identifier(): Column<R, T> {
        isIdentifier = true
        return this
    }

    open fun idString(row: R) = getter(row).toString()
}
