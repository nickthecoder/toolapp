/*
ParaTask Copyright (C) 2017  Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.toolapp.project

import javafx.application.Platform
import javafx.scene.Node
import javafx.scene.input.TransferMode
import uk.co.nickthecoder.paratask.gui.DropFiles
import uk.co.nickthecoder.paratask.parameters.FileParameter
import uk.co.nickthecoder.paratask.parameters.MultipleParameter
import uk.co.nickthecoder.toolapp.ToolApp
import java.io.File

class ParametersTab(val parametersPane: ToolParametersPane) : MinorTab("Parameters") {

    init {
        content = parametersPane as Node

        // Allow files to be dragged to the ParametersTab is the unnamedParameter is either a FileParameter,
        // or a MultipleParameter of Files.
        val defaultParameter = parametersPane.tool.taskD.unnamedParameter
        if (defaultParameter is MultipleParameter<*, *>) {
            if (defaultParameter.factory() is FileParameter) {
                @Suppress("UNCHECKED_CAST")
                defaultParameter as MultipleParameter<File, *>
                DropFiles(TransferMode.LINK) { _, list ->
                    list.forEach { defaultParameter.addValue(it) }
                    parametersPane.run()
                }.applyTo(this)
            }
        } else if (defaultParameter is FileParameter) {
            DropFiles(TransferMode.LINK) { _, list ->
                list.firstOrNull()?.let { defaultParameter.value = it }
                parametersPane.run()
            }.applyTo(this)
        }
    }

    override fun focus() {
        if (parametersPane.tool.toolPane?.skipFocus != true) {
            Platform.runLater {
                ToolApp.logFocus("ParametersTab.focus. parametersPane.focus()")
                parametersPane.focus()
            }
        }
    }

    override fun selected() {
        if (parametersPane.tool.toolPane?.skipFocus != true) {
            ToolApp.logFocus("ParametersTab.selected focus()")
            focus()
        }
    }

    override fun deselected() {
    }
}

