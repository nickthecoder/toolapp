/*
ParaTask Copyright (C) 2017  Nick Robinson>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.toolapp.tools

import javafx.scene.image.ImageView
import uk.co.nickthecoder.paratask.TaskDescription
import uk.co.nickthecoder.toolapp.project.ToolAppAction
import uk.co.nickthecoder.toolapp.project.ToolAppActions
import uk.co.nickthecoder.toolapp.table.BooleanColumn
import uk.co.nickthecoder.toolapp.table.Column
import uk.co.nickthecoder.toolapp.table.ListTableTool
import uk.co.nickthecoder.toolapp.table.filter.RowFilter

class ShortcutsTool : ListTableTool<ToolAppAction>() {

    override val taskD = TaskDescription("shortcuts", description = "Keyboard Shortcuts")

    override val rowFilter = RowFilter(this, columns, ToolAppActions.EDIT_COPY)

    init {
        columns.add(Column<ToolAppAction, ImageView>("icon", label = "", getter = { action -> ImageView(action.findImage()) }))
        columns.add(Column<ToolAppAction, String>("name", getter = { action -> action.name }))
        columns.add(BooleanColumn("changed", getter = { action -> action.isChanged() }))
        columns.add(Column<ToolAppAction, String>("shortcut", width = 300, getter = { action -> action.shortcutString() }))
    }

    override fun run() {
        list.clear()
        list.addAll(ToolAppActions.nameToActionMap.values)
    }

}
