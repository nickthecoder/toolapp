/*
ParaTask Copyright (C) 2017  Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

package uk.co.nickthecoder.toolapp.tools.places

import javafx.scene.image.ImageView
import javafx.scene.input.TransferMode
import uk.co.nickthecoder.paratask.TaskDescription
import uk.co.nickthecoder.paratask.TaskParser
import uk.co.nickthecoder.paratask.gui.DragFilesHelper
import uk.co.nickthecoder.paratask.gui.DropFiles
import uk.co.nickthecoder.paratask.gui.applyDropHelper
import uk.co.nickthecoder.paratask.parameters.*
import uk.co.nickthecoder.paratask.util.FileLister
import uk.co.nickthecoder.paratask.util.HasDirectory
import uk.co.nickthecoder.paratask.util.currentDirectory
import uk.co.nickthecoder.toolapp.misc.AutoRefresh
import uk.co.nickthecoder.toolapp.misc.FileOperations
import uk.co.nickthecoder.toolapp.misc.Thumbnailer
import uk.co.nickthecoder.toolapp.misc.WrappedFile
import uk.co.nickthecoder.toolapp.project.*
import uk.co.nickthecoder.toolapp.table.*
import uk.co.nickthecoder.toolapp.table.filter.RowFilter
import uk.co.nickthecoder.toolapp.table.filter.SingleRowFilter
import java.io.File

class DirectoryTool() :
        AbstractTableTool<WrappedFile>(),
        HasDirectory,
        SyncDirectory,
        SingleRowFilter<WrappedFile> {

    constructor(dirs: List<File>) : this() {
        directoriesP.value = dirs
    }

    constructor(dir: File) : this() {
        directoriesP.value = listOf(dir)
    }

    override val taskD = TaskDescription(name = "directory", description = "List Directories")

    val directoriesP = MultipleParameter("directories", value = listOf(currentDirectory), minItems = 1) {
        FileParameter("dir", label = "Directory", expectFile = false, mustExist = true)
    }

    val filterGroupP = SimpleGroupParameter("filter")

    val onlyFilesP = BooleanParameter("onlyFiles", required = false, value = null)
    val extensionsP = MultipleParameter("extensions", label = "File Extensions") { StringParameter("") }
    val includeHiddenP = BooleanParameter("includeHidden", value = false)

    val compactP = BooleanParameter("compact", value = false)

    val autoRefreshP = BooleanParameter("autoRefresh", value = true,
            description = "Refresh the list when the contents of the directory changes")


    val autoRefresh = AutoRefresh { toolPane?.parametersPane?.run() }

    val thumbnailer = Thumbnailer()

    override val directory: File?
        get() {
            return selectedDirectoryTableResults()?.directory ?: directoriesP.value.firstOrNull()
        }

    /**
     * The results Map of directory to list of files listed for the directory.
     */
    var lists = mutableMapOf<File, List<WrappedFile>>()

    // Used to select the correct ResultsTab when refreshing the tool
    var latestDirectory: File? = null

    // When a directory is changed, we use this, rather than latestDirectory to choose which ResultsTab to select
    var selectDirectory: File? = null

    override val rowFilter = RowFilter(this, createColumns(), WrappedFile(File("")))

    init {

        filterGroupP.addParameters(onlyFilesP, extensionsP, includeHiddenP)
        taskD.addParameters(
                directoriesP, filterGroupP,
                thumbnailer.heightP, thumbnailer.directoryThumbnailP, compactP, autoRefreshP)
        taskD.unnamedParameter = directoriesP

    }

    fun createColumns(): List<Column<WrappedFile, *>> {
        val columns = mutableListOf<Column<WrappedFile, *>>()
        columns.add(Column<WrappedFile, ImageView>("icon", width = 24, label = "", getter = { thumbnailer.thumbnailImageView(it.file) }))
        columns.add(FileNameColumn<WrappedFile>("name", getter = { it.file }).identifier())
        if (compactP.value != true) {
            columns.add(TimestampColumn<WrappedFile>("modified", getter = { it.file.lastModified() }))
            columns.add(SizeColumn<WrappedFile>("size", getter = { it.file.length() }))
        }
        return columns
    }

    override fun syncDirectory(dir: File) {
        val oldValue = directoriesP.value
        if (directoriesP.innerParameters.isEmpty()) {
            directoriesP.addValue(dir)
        } else {
            directoriesP.innerParameters[0].value = dir
        }
        if (oldValue != directoriesP.value) {
            toolPane?.skipFocus = true
            toolPane?.parametersPane?.run()
        }
    }

    override fun loadProblem(parameterName: String, expression: String?, stringValue: String?) {
        if (parameterName == "directory") {
            directoriesP.clear()
            if (expression != null) {
                val inner = directoriesP.newValue()
                inner.expression = expression
            } else {
                directoriesP.addValue(File(stringValue))
            }
        } else if (listOf("depth", "includeBase", "enterHidden", "placesFile", "treeRoot", "foldSingleDirectories").contains(parameterName)) {
            // Do nothing - we no longer use these parameters
            // "depth", "includeBase","enterHidden" were for the rather naff "DirectoryTreeTool", which no longer exists.
            // "placesFile", "treeRoot" and "foldSingleDirectories" was removed when docking and DirectoryTreeTool was introduced.
        } else {
            super<AbstractTableTool>.loadProblem(parameterName, expression, stringValue)
        }
    }

    override fun run() {
        latestDirectory = selectDirectory ?: selectedDirectoryTableResults()?.directory
        selectDirectory = null

        directoriesP.value.filterNotNull().forEach { dir ->
            listDirectory(dir)
        }
        updateTitle()

        autoRefresh.unwatchAll()
        if (autoRefreshP.value == true) {
            directoriesP.value.filterNotNull().forEach {
                autoRefresh.watch(it)
            }
        }

    }

    override fun attached(toolPane: ToolPane) {
        super.attached(toolPane)

        toolPane.createAddTabButton {
            onAddDirectory()
        }?.applyDropHelper(DropFiles(arrayOf(TransferMode.COPY)) { _, files ->
            files.filter { it.isDirectory }.forEach {
                addDirectory(it)
            }
        })
    }

    override fun detaching() {
        super.detaching()
        autoRefresh.unwatchAll()
    }

    fun createResults(dirP: FileParameter): Results {
        val dir = dirP.value!!
        val list = lists[dir]!!
        val tableResults = DirectoryTableResults(dir, list)

        return if (toolPane?.halfTab?.isDock() == true)
            tableResults
        else
            ResultsWithHeader(tableResults, Header(this, dirP))
    }

    override fun createResults(): List<Results> {
        return directoriesP.innerParameters.filter { it.value != null }.map { dirP ->
            createResults(dirP)
        }
    }

    fun updateTitle() {
        shortTitle = directory?.name ?: "Directory"
        longTitle = "Directory ${directory?.path}"
    }

    fun listDirectory(directory: File) {
        val lister = FileLister(
                onlyFiles = onlyFilesP.value,
                includeHidden = includeHiddenP.value!!,
                extensions = extensionsP.value
        )

        lists[directory] = lister.listFiles(directory).map { WrappedFile(it) }

    }

    fun selectedDirectoryTableResults(): DirectoryTableResults? {
        var res = toolPane?.currentResults()
        if (res is ResultsWithHeader) {
            res = res.results
        }
        if (res is DirectoryTableResults) {
            return res
        }
        return null
    }

    fun onAddDirectory() {
        val newFileP = directoriesP.addValue(null)

        focusOnParameter(newFileP)
    }

    fun addDirectory(directory: File) {
        directoriesP.addValue(directory)
        selectDirectory = directory
        toolPane?.parametersPane?.run()
    }

    fun changeDirectory(directory: File) {
        val oldDirectory = selectedDirectoryTableResults()?.directory
        println("Replacing $oldDirectory with $directory")
        if (oldDirectory != directory) {
            directoriesP.replace(oldDirectory, directory)
            selectDirectory = directory
            toolPane?.parametersPane?.run()
            println("Run!")
        }
    }


    inner class DirectoryDropHelper(val directory: File) : TableDropFilesHelper<WrappedFile>() {

        override fun acceptDropOnRow(row: WrappedFile): Array<TransferMode>? = if (row.isDirectory()) TransferMode.ANY else null

        override fun acceptDropOnNonRow(): Array<TransferMode> = TransferMode.ANY

        override fun droppedOnRow(row: WrappedFile, content: List<File>, transferMode: TransferMode) {
            if (row.isDirectory()) {
                FileOperations.instance.fileOperation(content, row.file, transferMode)
            }
        }

        override fun droppedOnNonRow(content: List<File>, transferMode: TransferMode) {
            FileOperations.instance.fileOperation(content, directory, transferMode)
        }
    }


    inner class DirectoryTableResults(val directory: File, list: List<WrappedFile>)
        : TableResults<WrappedFile>(this@DirectoryTool, list, directory.name, createColumns(), rowFilter = rowFilter, canClose = true) {

        init {
            dropHelper = DirectoryDropHelper(directory)

            dragHelper = DragFilesHelper {
                selectedRows().map { it.file }
            }
        }

        override fun attached(resultsTab: ResultsTab, toolPane: ToolPane) {
            super.attached(resultsTab, toolPane)
            if (latestDirectory == directory) {
                resultsTab.isSelected = true
            }
        }

        override fun closed() {
            directoriesP.remove(directory)
            // Hitting "Back" will un-close the tab ;-)
            toolPane?.halfTab?.pushHistory()
        }
    }

}


fun main(args: Array<String>) {
    TaskParser(DirectoryTool()).go(args)
}
