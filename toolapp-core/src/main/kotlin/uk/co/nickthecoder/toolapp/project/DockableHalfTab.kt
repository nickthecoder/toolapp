/*
ToolApp Copyright (C) 2019  Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.toolapp.project

import javafx.beans.property.SimpleStringProperty
import javafx.beans.property.StringProperty
import javafx.event.ActionEvent
import javafx.event.EventHandler
import javafx.scene.Node
import javafx.scene.control.*
import javafx.scene.image.Image
import javafx.scene.layout.BorderPane
import javafx.stage.Stage
import uk.co.nickthecoder.harbourfx.AbstractTabbedDockable
import uk.co.nickthecoder.harbourfx.Harbour
import uk.co.nickthecoder.harbourfx.NullDockableFactory
import uk.co.nickthecoder.paratask.AbstractTask
import uk.co.nickthecoder.paratask.TaskDescription
import uk.co.nickthecoder.paratask.gui.TaskPrompter
import uk.co.nickthecoder.paratask.parameters.StringParameter
import uk.co.nickthecoder.paratask.util.Stoppable
import uk.co.nickthecoder.paratask.util.requestFocusWhenSceneSet
import uk.co.nickthecoder.toolapp.Tool

class ToolAppHarbour(val projectWindow: ProjectWindow, center: Node)
    : Harbour(center, NullDockableFactory()), HalfTabParent {

    fun createDockedHalfTab(tool: Tool, run: Boolean = true): DockableHalfTab {
        val dht = DockableHalfTab(this, tool)
        dht.attached(this)
        if (run) {
            tool.toolPane?.parametersPane?.run()
        }
        return dht
    }
}

class DockableHalfTab(val harbour: ToolAppHarbour, tool: Tool)

    : HalfTab, AbstractTabbedDockable() {

    // HalfTab

    override val toolPane = DockableToolPane(this, tool)

    override val history = History(this)

    override val optionsField = TextField()

    override val toolBars = BorderPane()


    // Dockable

    override val dockableIcon: Image? = toolPane.tool.icon

    override val dockableId = "DockHalfTab:${dockCounter++}"

    override val dockableTitle: StringProperty = SimpleStringProperty(toolPane.tool.shortTitle)

    override val dockablePrefix: StringProperty = SimpleStringProperty("")

    // Tabbed Dockable

    override val noTabContent: Node
        get() = Label("No content")

    override fun rebuildTabs() {}

    // HalfTab


    override fun attached(halfTabParent: HalfTabParent) {
        toolPane.attached(this)
    }

    override fun detaching() {
        toolPane.detaching()
        toolBars.center = null
        toolBars.children.clear()
    }

    override fun focusOption() {
        optionsField.requestFocusWhenSceneSet()
    }

    override fun focusOtherHalf() {}

    override fun isLeft(): Boolean = true

    override fun isSelectedTab(): Boolean = false


    override fun onStop() {
        val tool = toolPane.tool
        if (tool is Stoppable) {
            tool.stop()
        }
    }

    override fun onRun() {
        toolPane.parametersPane.run()
    }

    override fun otherHalf(): HalfTab? = null

    override fun addAfter(newTool: Tool, run: Boolean, select: Boolean) {
        projectWindow().tabs.addTool(newTool, run, select)
    }

    override fun changeTool(tool: Tool, prompt: Boolean) {
        projectWindow().tabs.addTool(tool, !prompt, select = true)
    }

    override fun pushHistory() {}

    override fun pushHistory(tool: Tool) {}

    override fun selectTab() {}

    override fun close() {}


    override fun projectWindow() = harbour.projectWindow

    override fun isDock() = true

    override fun settingsMenu(): ContextMenu? {
        val menu = ContextMenu()

        menu.items.addAll(
                MenuItem("Undock").apply {
                    onAction = EventHandler {
                        addAfter(toolPane.tool.copy())
                        harbour.find(this@DockableHalfTab.dockableId)?.first?.remove(this@DockableHalfTab)
                    }
                },

                MenuItem("Close").apply {
                    onAction = EventHandler {
                        harbour.find(this@DockableHalfTab.dockableId)?.first?.remove(this@DockableHalfTab)
                    }
                },

                MenuItem("Rename").apply {
                    onAction = EventHandler {
                        TaskPrompter(RenameDockedHalfTab()).placeOnStage(Stage())
                    }
                },

                MenuItem("Parameters").apply {
                    onAction = EventHandler {
                        harbour.findOwner(this@DockableHalfTab)?.show(this@DockableHalfTab)
                        selectionModel.select(Tab(toolPane.parametersTab.text, toolPane.parametersTab.content))
                    }
                }

        )

        toolPane.filterTab?.let { filterTab ->
            menu.items.add(
                    MenuItem("Filter").apply {
                        onAction = EventHandler {
                            harbour.findOwner(this@DockableHalfTab)?.show(this@DockableHalfTab)
                            selectionModel.select(Tab(filterTab.text, filterTab.content))
                        }
                    }
            )
        }

        return menu
    }

    fun selectedResultsTab(): ResultsTab? {
        return toolPane.tabs.firstOrNull { it.content === selectionModel.selectedItem?.content } as? ResultsTab
    }

    override fun toString(): String = "DockHalfTab of ${toolPane.tool.shortTitle}"

    companion object {
        /**
         * Used to create a unique id for each Dockable
         */
        private var dockCounter = 0
    }


    inner class RenameDockedHalfTab : AbstractTask(false) {

        val titleP = StringParameter("title")

        override val taskD = TaskDescription("renameDockedTool")
                .addParameters(titleP)

        override fun run() {
            dockableTitle.value = titleP.value
        }
    }

}

class DockableToolPane(private val dockableHalfTab: DockableHalfTab, tool: Tool) : AbstractToolPane(tool) {

    override val tabs = mutableListOf<MinorTab>()

    override val selectionModel = object : SingleSelectionModel<MinorTab>() {
        override fun getItemCount(): Int = tabs.size
        override fun getModelItem(index: Int): MinorTab = tabs.get(index)
    }

    override fun removeResults(results: Results): Int {
        tabs.forEachIndexed { index, tab ->
            if (tab is ResultsTab && tab.results == results) {
                tabs.removeAt(index)
                dockableHalfTab.tabs.removeAt(index)
                return index
            }
        }
        return -1
    }

    override fun addResults(results: Results, index: Int): ResultsTab {
        val resultsTab = ResultsTab(results)
        resultsTab.canClose = results.canClose

        tabs.add(index, resultsTab)
        val newTab = Tab(resultsTab.text, resultsTab.content)
        dockableHalfTab.tabs.add(index, newTab)
        results.attached(resultsTab, this)
        if (dockableHalfTab.selectedResultsTab() == null) {
            dockableHalfTab.selectionModel.select(newTab)
        }
        if (selectionModel.selectedItem == null) {
            selectionModel.select(resultsTab)
        }
        return resultsTab
    }

    override fun createAddTabButton(action: (ActionEvent) -> Unit): Button? = null

}
