/*
ToolApp Copyright (C) 2019  Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.toolapp.tools.places

import javafx.scene.Node
import uk.co.nickthecoder.paratask.TaskDescription
import uk.co.nickthecoder.paratask.parameters.BooleanParameter
import uk.co.nickthecoder.paratask.parameters.FileParameter
import uk.co.nickthecoder.paratask.util.HasDirectory
import uk.co.nickthecoder.paratask.util.homeDirectory
import uk.co.nickthecoder.paratask.util.requestFocusWhenSceneSet
import uk.co.nickthecoder.toolapp.AbstractTool
import uk.co.nickthecoder.toolapp.misc.WrappedFile
import uk.co.nickthecoder.toolapp.options.RowOptionsRunner
import uk.co.nickthecoder.toolapp.project.AbstractResults
import uk.co.nickthecoder.toolapp.project.Results
import java.io.File

class DirectoryTreeTool :
        AbstractTool(),
        HasDirectory {


    val rootP = FileParameter("root", required = false, expectFile = false)

    val includeFiles = BooleanParameter("includeFiles", value = true)

    val includeHidden = BooleanParameter("includeHidden", value = false)

    val foldSingleDirectoriesP = BooleanParameter("foldSingleDirectories", value = true)

    override val taskD = TaskDescription(name = "directoryTree", description = "Directory Tree")
            .addParameters(rootP, includeHidden, includeFiles, foldSingleDirectoriesP)

    override val directory: File?
        get() = rootP.value


    init {
        taskD.unnamedParameter = rootP
    }


    override fun createResults(): List<Results> {
        return listOf(DirectoryTreeResults())
    }

    override fun run() {
    }

    inner class DirectoryTreeResults : AbstractResults(this@DirectoryTreeTool) {

        val runner = RowOptionsRunner<WrappedFile>(tool)

        val tree = DirectoryTree(rootP.value ?: homeDirectory,
                includeHidden = includeHidden.value!!,
                includeFiles = includeFiles.value!!,
                foldSingleDirectories = foldSingleDirectoriesP.value!!,
                runner = runner)

        override val node: Node = tree

        var foldSingleDirectories: Boolean
            get() = tree.foldSingleDirectories
            set(v) {
                tree.foldSingleDirectories = v
            }

        override fun focus() {
            tree.requestFocusWhenSceneSet()
        }

        fun update() {
            foldSingleDirectories = foldSingleDirectoriesP.value == true
            tree.rootDirectory = rootP.value ?: homeDirectory
        }
    }

}
