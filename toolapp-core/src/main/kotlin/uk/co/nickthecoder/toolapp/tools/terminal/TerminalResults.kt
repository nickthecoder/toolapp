/*
ParaTask Copyright (C) 2017  Nick Robinson>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.toolapp.tools.terminal

import uk.co.nickthecoder.paratask.util.Stoppable
import uk.co.nickthecoder.paratask.util.process.OSCommand
import uk.co.nickthecoder.toolapp.Tool
import uk.co.nickthecoder.toolapp.project.Results

interface TerminalResults : Results, Stoppable {

    fun start(osCommand: OSCommand)

    fun waitFor(): Int

    val process: Process?

    companion object {
        fun create(tool: Tool, showCommand: Boolean, allowInput: Boolean): TerminalResults {
            // Let's try to create a RealTerminalResults, but do it using reflection so that this code can be compiled
            // without all of the bloat required by JediTerm. Therefore, we have a choice of lots of bloat, but an
            // excellent terminal, or no bloat, and a naff terminal.
            try {
                // The following is a reflection version of : return RealTerminalResults(this)

                println( "Looking for RealTerminalResults")
                val realResultsClass = Class.forName("uk.co.nickthecoder.toolapp.tools.terminal.RealTerminalResults")
                val constructor = realResultsClass.getConstructor(Tool::class.java)
                return constructor.newInstance(tool) as TerminalResults

            } catch (e: Exception) {
                println("Real terminal failed")
                e.printStackTrace()
                // Fall back to using the naff, SimpleTerminalResults
                return SimpleTerminalResults(tool, showCommand = showCommand, allowInput = allowInput)
            }
        }
    }
}
