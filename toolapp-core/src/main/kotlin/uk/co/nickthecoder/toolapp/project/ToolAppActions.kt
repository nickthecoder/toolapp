/*
ParaTask Copyright (C) 2017  Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

package uk.co.nickthecoder.toolapp.project

import com.eclipsesource.json.Json
import com.eclipsesource.json.JsonArray
import com.eclipsesource.json.JsonObject
import com.eclipsesource.json.PrettyPrint
import javafx.scene.input.KeyCode
import javafx.scene.input.KeyCodeCombination
import javafx.scene.input.KeyCombination
import uk.co.nickthecoder.paratask.util.child
import java.io.*

object ToolAppActions {

    val nameToActionMap = mutableMapOf<String, ToolAppAction>()

    // ProjectWindow

    val PROJECT_OPEN = ToolAppAction("project.open", KeyCode.O, alt = true, tooltip = "Open Project")
    val PROJECT_SAVE = ToolAppAction("project.save", KeyCode.S, alt = true, tooltip = "Save Project")
    val QUIT = ToolAppAction("application.quit", KeyCode.Q, alt = true, label = "Quit", tooltip = "Quit Para Task")
    val WINDOW_NEW = ToolAppAction("window.new", KeyCode.N, alt = true, tooltip = "New Window")
    val APPLICATION_ABOUT = ToolAppAction("application.about", KeyCode.F1, tooltip = "About ParaTask")

    // ProjectTab

    val TAB_NEW = ToolAppAction("tab.new", KeyCode.T, control = true, label = "New Tab")
    val TAB_NEW_TERMINAL = ToolAppAction("tab.new.terminal", KeyCode.PERIOD, control = true, tooltip = "New Terminal")
    val TAB_PROPERTIES = ToolAppAction("tab.properties", null, label = "Properties")
    val TAB_DUPLICATE = ToolAppAction("tab.duplicate", KeyCode.D, alt = true, label = "Duplicate Tab", tooltip = "Duplicate Tab")
    val TAB_CLOSE = ToolAppAction("tab.close", null, label = "Close Tab")
    val TAB_RESTORE = ToolAppAction("tab.restore", KeyCode.T, shift = true, control = true, label = "Restore Tab")
    val TAB_SPLIT_ORIENTATION = ToolAppAction("tab.split.orientation", KeyCode.BACK_SLASH, control = true, label = "Change Split Orientation")
    val TAB_DOCK = ToolAppAction("tab.dock", KeyCode.COMMA, control = true, label = "Move to Dock")

    // HalfTab

    val RESULTS_TAB_CLOSE = ToolAppAction("results-tab.close", KeyCode.W, alt = true, label = "Close Results Tab")

    val TOOL_STOP = ToolAppAction("tool.stop", KeyCode.ESCAPE, shift = true, tooltip = "Stop the Tool")
    val TOOL_RUN = ToolAppAction("tool.run", KeyCode.F5, tooltip = "(Re) Run the Tool")

    val TOOL_SELECT = ToolAppAction("tool.select", KeyCode.HOME, control = true, tooltip = "Select a Tool")
    val TOOL_CLOSE = ToolAppAction("tool.close", KeyCode.W, control = true, shift = false, tooltip = "Close Tool")

    val HISTORY_BACK = ToolAppAction("history.back", KeyCode.LEFT, alt = true, tooltip = "Back")
    val HISTORY_FORWARD = ToolAppAction("history.forward", KeyCode.RIGHT, alt = true, tooltip = "Forward")

    val TAB_MERGE_TOGGLE = ToolAppAction("tab.merge.toggle", null, tooltip = "Split / Merge with the Tab to the right")
    val TAB_SPLIT_TOGGLE = ToolAppAction("tab.split.toggle", KeyCode.F3, tooltip = "Split/Un-Split")
    val TAB_SPLIT_NEW = ToolAppAction("tab.split.new", KeyCode.SLASH, control = true, tooltip = "Split")
    val TAB_SPLIT_TERMINAL = ToolAppAction("tab.split.terminal", KeyCode.PERIOD, control = true, shift = true, tooltip = "Split Terminal")

    val OPTION_RUN = ToolAppAction("actions.run", KeyCode.ENTER)
    val OPTION_RUN_NEW_TAB = ToolAppAction("actions.run.new.tab", KeyCode.ENTER, shift = true)
    val OPTION_PROMPT = ToolAppAction("actions.run", KeyCode.F8)
    val OPTION_PROMPT_NEW_TAB = ToolAppAction("actions.run.new.tab", KeyCode.F8, shift = true)

    val PARAMETERS_SHOW = ToolAppAction("parameters.show", KeyCode.P, control = true, label = "Show Parameters")
    val RESULTS_SHOW = ToolAppAction("results.show", KeyCode.R, control = true, label = "Show Results")

    // Table

    val NEXT_ROW = ToolAppAction("row.next", KeyCode.DOWN)
    val PREV_ROW = ToolAppAction("row.previous", KeyCode.UP)
    val SELECT_ROW_DOWN = ToolAppAction("row.select.down", KeyCode.DOWN, shift = true)
    val SELECT_ROW_UP = ToolAppAction("row.select.up", KeyCode.UP, shift = true)

    // EditorTool

    val EDIT_FIND = ToolAppAction("edit.find", KeyCode.F, control = true, tooltip = "Find")
    val EDIT_REPLACE = ToolAppAction("edit.replace", KeyCode.H, control = true, tooltip = "Replace")

    // General

    val CONTEXT_MENU = ToolAppAction("context.menu", KeyCode.CONTEXT_MENU)
    val FILE_SAVE = ToolAppAction("file.save", KeyCode.S, control = true, tooltip = "Save")
    val EDIT_CUT = ToolAppAction("edit.cut", KeyCode.X, control = true, tooltip = "Cut")
    val EDIT_COPY = ToolAppAction("edit.copy", KeyCode.C, control = true, tooltip = "Copy")
    val EDIT_PASTE = ToolAppAction("edit.paste", KeyCode.V, control = true, tooltip = "Paste")
    val EDIT_UNDO = ToolAppAction("edit.undo", KeyCode.Z, control = true, tooltip = "Undo")
    val EDIT_REDO = ToolAppAction("edit.redo", KeyCode.Z, shift = true, control = true, tooltip = "Redo")
    val ESCAPE = ToolAppAction("escape", KeyCode.ESCAPE)
    val SELECT_ALL = ToolAppAction("select-all", KeyCode.A, control = true)
    val SELECT_NONE = ToolAppAction("select-all", KeyCode.A, control = true, shift = true)

    // GlobalShortcuts

    val NEXT_MAJOR_TAB = ToolAppAction("tab.major.next", KeyCode.CLOSE_BRACKET, control = true)
    val PREV_MAJOR_TAB = ToolAppAction("tab.major.prev", KeyCode.OPEN_BRACKET, control = true)
    val NEXT_MINOR_TAB = ToolAppAction("tab.minor.next", KeyCode.CLOSE_BRACKET, control = true, shift = true)
    val PREV_MINOR_TAB = ToolAppAction("tab.minor.prev", KeyCode.OPEN_BRACKET, control = true, shift = true)

    val FOCUS_OPTION = ToolAppAction("focus.option", KeyCode.F10, control = true)
    val FOCUS_RESULTS = ToolAppAction("focus.results", KeyCode.F10)
    val FOCUS_HEADER = ToolAppAction("focus.header", KeyCode.F10, shift = true)
    val FOCUS_OTHER_SPLIT = ToolAppAction("focus.other.split", KeyCode.F3, control = true)

    val MAJOR_TAB_1 = ToolAppAction("tab.major.1", KeyCode.DIGIT1, control = true)
    val MAJOR_TAB_2 = ToolAppAction("tab.major.2", KeyCode.DIGIT2, control = true)
    val MAJOR_TAB_3 = ToolAppAction("tab.major.3", KeyCode.DIGIT3, control = true)
    val MAJOR_TAB_4 = ToolAppAction("tab.major.4", KeyCode.DIGIT4, control = true)
    val MAJOR_TAB_5 = ToolAppAction("tab.major.5", KeyCode.DIGIT5, control = true)
    val MAJOR_TAB_6 = ToolAppAction("tab.major.6", KeyCode.DIGIT6, control = true)
    val MAJOR_TAB_7 = ToolAppAction("tab.major.7", KeyCode.DIGIT7, control = true)
    val MAJOR_TAB_8 = ToolAppAction("tab.major.8", KeyCode.DIGIT8, control = true)
    val MAJOR_TAB_9 = ToolAppAction("tab.major.9", KeyCode.DIGIT9, control = true)

    val MAJOR_TABS = listOf(MAJOR_TAB_1, MAJOR_TAB_2, MAJOR_TAB_3, MAJOR_TAB_4, MAJOR_TAB_5, MAJOR_TAB_6, MAJOR_TAB_7, MAJOR_TAB_8, MAJOR_TAB_9)

    val MINOR_TAB_1 = ToolAppAction("tab.minor.1", KeyCode.DIGIT1, control = true, shift = true)
    val MINOR_TAB_2 = ToolAppAction("tab.minor.2", KeyCode.DIGIT2, control = true, shift = true)
    val MINOR_TAB_3 = ToolAppAction("tab.minor.3", KeyCode.DIGIT3, control = true, shift = true)
    val MINOR_TAB_4 = ToolAppAction("tab.minor.4", KeyCode.DIGIT4, control = true, shift = true)
    val MINOR_TAB_5 = ToolAppAction("tab.minor.5", KeyCode.DIGIT5, control = true, shift = true)
    val MINOR_TAB_6 = ToolAppAction("tab.minor.6", KeyCode.DIGIT6, control = true, shift = true)
    val MINOR_TAB_7 = ToolAppAction("tab.minor.7", KeyCode.DIGIT7, control = true, shift = true)
    val MINOR_TAB_8 = ToolAppAction("tab.minor.8", KeyCode.DIGIT8, control = true, shift = true)
    val MINOR_TAB_9 = ToolAppAction("tab.minor.9", KeyCode.DIGIT9, control = true, shift = true)

    val MINOR_TABS = listOf(MINOR_TAB_1, MINOR_TAB_2, MINOR_TAB_3, MINOR_TAB_4, MINOR_TAB_5, MINOR_TAB_6, MINOR_TAB_7, MINOR_TAB_8, MINOR_TAB_9)

    init {
        load()
    }

    fun add(action: ToolAppAction) {
        nameToActionMap[action.name] = action
    }

    fun shortcutPreferencesFile() = Preferences.configDirectory.child("shortcuts.json")

    fun save() {

        val jroot = JsonObject()
        val jshortcuts = JsonArray()

        nameToActionMap.values.forEach { action ->
            if (action.isChanged()) {
                val jshortcut = JsonObject()

                jshortcut.add("name", action.name)
                jshortcut.add("keycode", action.keyCodeCombination?.code?.toString() ?: "")
                addModifier(jshortcut, "control", action.keyCodeCombination?.control)
                addModifier(jshortcut, "shift", action.keyCodeCombination?.shift)
                addModifier(jshortcut, "alt", action.keyCodeCombination?.alt)

                jshortcuts.add(jshortcut)
            }
        }
        jroot.add("shortcuts", jshortcuts)

        BufferedWriter(OutputStreamWriter(FileOutputStream(shortcutPreferencesFile()))).use {
            jroot.writeTo(it, PrettyPrint.indentWithSpaces(4))
        }
    }

    fun addModifier(jparent: JsonObject, name: String, mod: KeyCombination.ModifierValue?) {
        mod?.let { jparent.add(name, mod.toString()) }
    }

    fun load() {

        val file = shortcutPreferencesFile()
        if (!file.exists()) {
            return
        }

        val jroot = Json.parse(InputStreamReader(FileInputStream(file))).asObject()
        val jshortcutsObj = jroot.get("shortcuts")
        jshortcutsObj?.let {
            val jshortcuts = it.asArray()
            jshortcuts.forEach {
                val jshortcut = it.asObject()
                val name = jshortcut.getString("name", "")
                val action = nameToActionMap[name]
                action?.let {
                    val keyCodeS = jshortcut.getString("keycode", "")
                    val controlS = jshortcut.getString("control", "ANY")
                    val shiftS = jshortcut.getString("shift", "ANY")
                    val altS = jshortcut.getString("alt", "ANY")

                    val control = KeyCombination.ModifierValue.valueOf(controlS)
                    val shift = KeyCombination.ModifierValue.valueOf(shiftS)
                    val alt = KeyCombination.ModifierValue.valueOf(altS)

                    if (keyCodeS == "") {
                        it.keyCodeCombination = null
                    } else {
                        val keyCode = KeyCode.valueOf(keyCodeS)
                        it.keyCodeCombination = KeyCodeCombination(
                                keyCode,
                                control,
                                shift,
                                alt,
                                KeyCombination.ModifierValue.UP,
                                KeyCombination.ModifierValue.UP)
                    }
                }
            }
        }
    }

}
