/*
ParaTask Copyright (C) 2017  Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

package uk.co.nickthecoder.toolapp.project

import javafx.scene.Node
import javafx.scene.Scene
import uk.co.nickthecoder.paratask.util.HasDirectory
import uk.co.nickthecoder.toolapp.ToolApp
import uk.co.nickthecoder.toolapp.tools.HomeTool
import uk.co.nickthecoder.toolapp.tools.terminal.TerminalTool

class GlobalShortcuts(val scene: Scene, val projectWindow: ProjectWindow) {

    init {
        put(ToolAppActions.TOOL_STOP) { halfTab()?.onStop() }
        put(ToolAppActions.TOOL_RUN) { halfTab()?.onRun() }
        put(ToolAppActions.TOOL_SELECT) { projectWindow.addTool(HomeTool()) }
        put(ToolAppActions.TOOL_CLOSE) { halfTab()?.close() }
        put(ToolAppActions.HISTORY_BACK) { halfTab()?.history?.undo() }
        put(ToolAppActions.HISTORY_FORWARD) { halfTab()?.history?.redo() }

        put(ToolAppActions.NEXT_MAJOR_TAB) { projectWindow.tabs.nextTab() }
        put(ToolAppActions.PREV_MAJOR_TAB) { projectWindow.tabs.prevTab() }
        put(ToolAppActions.NEXT_MINOR_TAB) { toolPane()?.nextTab() }
        put(ToolAppActions.PREV_MINOR_TAB) { toolPane()?.prevTab() }

        put(ToolAppActions.FOCUS_HEADER) {
            ToolApp.logFocus("GlobalShortcuts FOCUS_HEADER. toolPane().focusHeader()")
            toolPane()?.focusHeader()
        }
        put(ToolAppActions.FOCUS_RESULTS) {
            ToolApp.logFocus("GlobalShortcuts FOCUS_RESULTS. toolPane().focusResults()")
            toolPane()?.focusResults()
        }
        put(ToolAppActions.FOCUS_OPTION) {
            ToolApp.logFocus("GlobalShortcuts FOCUS_OPTION. halfTab().focusOption()")
            halfTab()?.focusOption()
        }

        put(ToolAppActions.FOCUS_OTHER_SPLIT) {
            ToolApp.logFocus("GlobalShortcuts FOCUS_OTHER_SPLIT.")
            val otherHalf = halfTab()?.otherHalf()
            if (otherHalf == null) {
                toolPane()?.focusResults()
            } else {
                otherHalf.toolPane.focusResults()
            }
        }

        put(ToolAppActions.TAB_NEW) { projectWindow.tabs.addTool(HomeTool()) }
        put(ToolAppActions.TAB_RESTORE) { projectWindow.tabs.restoreTab() }
        put(ToolAppActions.TAB_NEW_TERMINAL) { projectWindow.tabs.addTool(TerminalTool()) }
        put(ToolAppActions.TAB_SPLIT_TERMINAL) {
            projectWindow.tabs.currentTab()?.let { tab ->
                val tool = TerminalTool()
                val existingTool = tab.left.toolPane.tool
                (existingTool as? HasDirectory)?.directory?.let { tool.directoryP.value = it }

                if (tab.right == null) {
                    tab.split(tool)
                } else {
                    projectWindow.tabs.addTool(tool)
                }
            }
        }

        for ((index, action) in ToolAppActions.MAJOR_TABS.withIndex()) {
            put(action) { projectWindow.tabs.selectTab(index) }
        }

        for ((index, action) in ToolAppActions.MINOR_TABS.withIndex()) {
            put(action) { toolPane()?.selectTab(index) }
        }

    }

    fun put(action: ToolAppAction, call: () -> Unit) {
        scene.accelerators[action.keyCodeCombination] = Runnable { call() }
    }

    fun tab(): ProjectTab? = projectWindow.tabs.currentTab()

    fun halfTab(): HalfTab? {
        var node: Node? = scene.focusOwner
        while (node != null) {
            if (node is HalfTab) {
                return node
            }
            node = node.parent
        }

        return tab()?.left
    }

    fun toolPane(): ToolPane? = halfTab()?.toolPane


}
