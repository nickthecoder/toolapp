/*
ParaTask Copyright (C) 2017  Nick Robinson>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.toolapp.tools.places

import javafx.collections.ObservableList
import javafx.event.EventHandler
import javafx.geometry.Side
import javafx.scene.control.ContextMenu
import javafx.scene.control.TreeItem
import javafx.scene.control.TreeView
import javafx.scene.input.KeyEvent
import javafx.scene.input.MouseEvent
import uk.co.nickthecoder.paratask.gui.ApplicationActions
import uk.co.nickthecoder.paratask.gui.DragFilesHelper
import uk.co.nickthecoder.paratask.util.FileLister
import uk.co.nickthecoder.toolapp.misc.Thumbnailer
import uk.co.nickthecoder.toolapp.misc.WrappedFile
import uk.co.nickthecoder.toolapp.options.RowOptionsRunner
import java.io.File

class DirectoryTree(
        rootDirectory: File = File.listRoots()[0],
        val includeHidden: Boolean = false,
        val includeFiles: Boolean = false,
        foldSingleDirectories: Boolean = true,
        val runner: RowOptionsRunner<WrappedFile>)

    : TreeView<String>() {

    var rootDirectory: File = rootDirectory
        set(v) {
            if (v != field) {
                val oldRoot = root
                root = FileItem(v)
                reExpand(oldRoot as FileItem)
            }
        }

    var foldSingleDirectories: Boolean = foldSingleDirectories
        set(v) {
            field = v
            rebuild()
        }

    var onSelected: ((File) -> Unit)? = null

    val dragHelper = DragFilesHelper {
        val item = selectionModel.selectedItemProperty().get() as FileItem?
        if (item == null) {
            null
        } else {
            listOf(item.file)
        }
    }

    val selectedFile: File?
        get() = (selectionModel.selectedItem as FileItem?)?.file

    init {
        isEditable = false
        root = FileItem(rootDirectory)
        root.children

        root.isExpanded = true
        dragHelper.applyTo(this)

        onMousePressed = EventHandler { if (it.isPopupTrigger) popupTrigger(it) }
        onMouseReleased = EventHandler { if (it.isPopupTrigger) popupTrigger(it) }
        onMouseClicked = EventHandler { onMouseClicked(it) }
    }

    private fun onMouseClicked(event: MouseEvent) {
        val item = selectionModel.selectedItem as? FileItem
        if (event.clickCount == 2 && item?.isLeaf == true) {
            runner.runDefault(WrappedFile(item.file))
        }
    }

    private fun popupTrigger(event: MouseEvent) {
        onSelect()
        event.consume()
        selectedFile?.let { file ->
            contextMenu = ContextMenu()
            runner.buildContextMenu(contextMenu, listOf(WrappedFile(file)))
            contextMenu.show(this, Side.BOTTOM, event.x, 0.0)
        }
    }

    fun onKeyPressed(event: KeyEvent) {
        if (ApplicationActions.ENTER.match(event)) {
            onSelect()
            event.consume()
        } else if (ApplicationActions.SPACE.match(event)) {
            selectionModel.selectedItem?.let {
                it.isExpanded = !it.isExpanded
                event.consume()
            }
        }
    }

    fun onSelect() {
        onSelected?.let { handler ->
            selectedFile?.let {
                handler(it)
            }
        }
    }

    /**
     * Finds the item in the tree for a given directory, or null if it is not in the tree.
     */
    fun findItem(directory: File): FileItem? {
        val rootItem = root as FileItem

        if (rootItem.file == directory) {
            return rootItem

        } else {
            // List all of the parent directories up to the tree's root.
            val parents = mutableListOf<File>()
            var d1: File? = directory
            while (d1 != null) {
                if (d1 == rootDirectory) {
                    break
                }
                parents.add(0, d1)
                d1 = d1.parentFile
            }

            // Start at the root node, and look for child nodes that are in the list
            var item: FileItem? = rootItem
            while (item != null) {
                val found = item.children.filterIsInstance<FileItem>().firstOrNull { parents.contains(it.file) }
                        ?: return item
                item = found
            }
            return null
        }
    }

    /**
     * If directory is somewhere below rootDirectory, then expand the items as needed and select the item
     * corresponding to the directory given.
     */
    fun selectDirectory(directory: File): FileItem? {
        val item = findItem(directory)
        item?.let { selectionModel.select(item) }
        return item
    }


    private fun reExpand(oldItem: FileItem) {

        if (oldItem.isExpanded) {
            val foundItem = findItem(oldItem.file)
            foundItem?.isExpanded = true
            oldItem.children.filterIsInstance<FileItem>().forEach {

                if (!foldSingleDirectories) {
                    // Expand all previously folded items
                    var parent: File? = oldItem.file.parentFile
                    while (parent != oldItem.file && parent != null) {
                        findItem(parent)?.isExpanded = true
                        parent = parent.parentFile
                    }
                }

                reExpand(it)
            }
        }
    }

    fun rebuild() {
        val oldRoot = root as FileItem
        root = FileItem(rootDirectory)
        reExpand(oldRoot)
    }

    inner class FileItem(val file: File, label: String = file.name) : TreeItem<String>(label) {

        var firstTimeChildren = true

        init {
            if (file.isFile) {
                graphic = Thumbnailer.filetypeImageView(file)
            }
        }

        override fun getChildren(): ObservableList<TreeItem<String>> {
            val superChildren = super.getChildren()
            if (firstTimeChildren) {
                firstTimeChildren = false
                val lister = FileLister(onlyFiles = if (includeFiles) null else false, includeHidden = includeHidden)
                lister.listFiles(file).forEach { child ->
                    superChildren.add(createChild(child))
                }
            }

            return superChildren
        }

        private fun createChild(subFile: File, prefix: String = ""): FileItem {
            if (foldSingleDirectories) {
                val lister = FileLister(onlyFiles = null, includeHidden = includeHidden)

                val grandChildren = lister.listFiles(subFile)
                if (grandChildren.size == 1 && grandChildren[0].isDirectory) {
                    val newPrefix = prefix + subFile.name + File.separator
                    return createChild(grandChildren[0], prefix = newPrefix)
                }
            }
            return FileItem(subFile, prefix + subFile.name)
        }

        fun isFolded(): Boolean {
            return (parent as FileItem).file != file.parentFile
        }

        override fun isLeaf(): Boolean {
            return !file.isDirectory
        }

        override fun toString(): String = file.name

    }
}
