/*
ParaTask Copyright (C) 2017  Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

package uk.co.nickthecoder.toolapp.project

import javafx.application.Platform
import javafx.geometry.Side
import javafx.scene.Node
import javafx.scene.control.Button
import javafx.scene.control.ContextMenu
import javafx.scene.control.TextField
import javafx.scene.control.ToolBar
import javafx.scene.input.KeyEvent
import javafx.scene.input.MouseEvent
import javafx.scene.input.TransferMode
import javafx.scene.layout.BorderPane
import javafx.scene.layout.StackPane
import uk.co.nickthecoder.paratask.gui.CompoundButtons
import uk.co.nickthecoder.paratask.gui.DropFiles
import uk.co.nickthecoder.paratask.gui.ShortcutHelper
import uk.co.nickthecoder.paratask.gui.applyDropHelper
import uk.co.nickthecoder.paratask.util.Stoppable
import uk.co.nickthecoder.paratask.util.requestFocusWhenSceneSet
import uk.co.nickthecoder.toolapp.Tool
import uk.co.nickthecoder.toolapp.ToolApp
import uk.co.nickthecoder.toolapp.tools.HomeTool
import uk.co.nickthecoder.toolapp.tools.editor.EditorTool
import uk.co.nickthecoder.toolapp.tools.places.DirectoryTool

class HalfTab_Impl(tool: Tool)

    : BorderPane(), HalfTab {

    override var toolPane: ToolPane = ToolPane_Impl(tool)

    override val toolBars = BorderPane()

    private val toolBar = ToolBar()

    private val shortcuts = ShortcutHelper("HalfTab", this)

    override val optionsField = TextField()

    lateinit var projectTab: ProjectTab

    val stopButton: Button

    val runButton: Button

    override val history = History(this)

    val optionsContextMenu = ContextMenu()

    init {
        toolBars.center = toolBar
        toolBar.styleClass.add("bottom")

        center = toolPane as Node
        bottom = toolBars

        with(optionsField) {
            prefColumnCount = 6
            addEventHandler(KeyEvent.KEY_PRESSED) { optionsFieldKeyPressed(it) }
            addEventHandler(MouseEvent.MOUSE_PRESSED, { optionFieldMouse(it) })
            addEventHandler(MouseEvent.MOUSE_RELEASED, { optionFieldMouse(it) })
            contextMenu = optionsContextMenu
        }

        val historyGroup = CompoundButtons()
        val backButton = ToolAppActions.HISTORY_BACK.createButton(shortcuts) { history.undo() }
        val forwardButton = ToolAppActions.HISTORY_FORWARD.createButton(shortcuts) { history.redo() }
        backButton.disableProperty().bind(history.canUndoProperty.not())
        forwardButton.disableProperty().bind(history.canRedoProperty.not())
        historyGroup.children.addAll(backButton, forwardButton)

        val runStopStack = StackPane()
        stopButton = ToolAppActions.TOOL_STOP.createButton(shortcuts) { onStop() }
        runButton = ToolAppActions.TOOL_RUN.createButton(shortcuts) { onRun() }
        runStopStack.children.addAll(stopButton, runButton)

        // Only show ONE of tabSplitToggleButton and tabSplitNewButton.
        val tabSplitToggleButton = ToolAppActions.TAB_SPLIT_TOGGLE.createButton(shortcuts) { projectTab.splitToggle() }
        val tabSplitNewButton = ToolAppActions.TAB_SPLIT_NEW.createToolButton(
                shortcuts,
                { toolPane.tool.copy() },
                { otherTool -> projectTab.split(otherTool) }
        )
        val tabSplitStackPane = StackPane(tabSplitToggleButton)
        // Not attached yet, so use runLater as a quick workaround.
        Platform.runLater {
            val splitPane = (projectTab as ProjectTab_Impl).splitPane
            splitPane.itemCount().addListener { _, _, newValue ->
                tabSplitStackPane.children[0] = if (newValue == 2) tabSplitToggleButton else tabSplitNewButton
            }
            tabSplitStackPane.children[0] = if (splitPane.itemCount == 2) tabSplitToggleButton else tabSplitNewButton
        }
        // Allow files/directories to be dragged
        tabSplitNewButton.applyDropHelper(DropFiles(TransferMode.ANY) { _, filesAndDirs ->
            val files = filesAndDirs.filter { it.isFile }
            val dirs = filesAndDirs.filter { it.isDirectory }
            if (files.isNotEmpty()) {
                projectTab.split(EditorTool(files))
            } else if (dirs.isNotEmpty()) {
                projectTab.split(DirectoryTool(dirs))
            }
        })

        toolBar.items.addAll(
                optionsField,
                runStopStack,
                ToolAppActions.TOOL_SELECT.createToolButton(shortcuts, { HomeTool() }) { otherTool -> onSelectTool(otherTool) },
                historyGroup,
                tabSplitStackPane,
                ToolAppActions.TAB_MERGE_TOGGLE.createButton(shortcuts) { projectTab.mergeToggle() },
                ToolAppActions.TOOL_CLOSE.createButton(shortcuts) { close() })

        bindButtons()
        shortcuts.add(ToolAppActions.PARAMETERS_SHOW) { onShowParameters() }
        shortcuts.add(ToolAppActions.RESULTS_SHOW) { onShowResults() }
        shortcuts.add(ToolAppActions.RESULTS_TAB_CLOSE) { onCloseResults() }

    }

    override fun attached(halfTabParent: HalfTabParent) {
        halfTabParent as? ProjectTab
                ?: throw IllegalArgumentException("Expected a ProjectTab, but found ${halfTabParent.javaClass}")
        projectTab = halfTabParent

        ToolApp.logAttach("HalfTab.attaching ToolPane")
        toolPane.attached(this)
        ToolApp.logAttach("HalfTab.attached ToolPane")
    }

    override fun detaching() {
        ToolApp.logAttach("HalfTab.detaching ToolPane")
        toolPane.detaching()
        center = null
        bottom = null

        toolBars.children.clear()
        toolBar.items.clear()
        shortcuts.clear()
        optionsContextMenu.items.clear()
        ToolApp.logAttach("HalfTab.detached ToolPane")
    }

    override fun isLeft() = projectTab.left === this

    override fun otherHalf(): HalfTab? {
        return if (isLeft()) {
            projectTab.right
        } else {
            projectTab.left
        }
    }

    fun bindButtons() {
        runButton.disableProperty().bind(toolPane.tool.taskRunner.disableRunProperty)
        runButton.visibleProperty().bind(toolPane.tool.taskRunner.showRunProperty)
        stopButton.visibleProperty().bind(toolPane.tool.taskRunner.showStopProperty)
    }

    override fun changeTool(tool: Tool, prompt: Boolean) {

        toolPane.detaching()
        children.remove(toolPane as Node)

        toolPane = ToolPane_Impl(tool)

        center = toolPane as Node
        toolPane.attached(this)

        projectTab.changed()

        bindButtons()
        history.push(tool)

        if (!prompt) {
            try {
                tool.check()
            } catch (e: Exception) {
                return
            }
            toolPane.parametersPane.run()
        }
    }

    override fun onStop() {
        val tool = toolPane.tool
        if (tool is Stoppable) {
            tool.stop()
        }
    }

    override fun onRun() {
        toolPane.parametersPane.run()
    }

    fun onSelectTool(tool: Tool) {
        val newTool = tool.copy()
        newTool.resolveParameters(projectTab.projectTabs.projectWindow.project.resolver)
        changeTool(newTool)
    }

    override fun close() {
        projectTab.remove(toolPane)
    }

    override fun pushHistory() {
        history.push(toolPane.tool)
    }

    override fun pushHistory(tool: Tool) {
        history.push(tool)
    }

    fun optionsFieldKeyPressed(event: KeyEvent) {
        var done = false
        val tool = toolPane.resultsTool()
        val runner = tool.optionsRunner

        when {
            ToolAppActions.OPTION_RUN.match(event) -> done = runner.runNonRow(optionsField.text, prompt = false, newTab = false)
            ToolAppActions.OPTION_RUN_NEW_TAB.match(event) -> done = runner.runNonRow(optionsField.text, prompt = false, newTab = true)
            ToolAppActions.OPTION_PROMPT.match(event) -> done = runner.runNonRow(optionsField.text, prompt = true, newTab = false)
            ToolAppActions.OPTION_PROMPT_NEW_TAB.match(event) -> done = runner.runNonRow(optionsField.text, prompt = true, newTab = true)
            ToolAppActions.CONTEXT_MENU.match(event) -> {
                onOptionsContextMenu()
                event.consume()
            }
        }

        if (done) {
            optionsField.text = ""
        }
    }

    fun optionFieldMouse(event: MouseEvent) {
        if (event.isPopupTrigger) {
            onOptionsContextMenu()
            event.consume()
        }
    }

    private fun onOptionsContextMenu() {
        val tool = toolPane.resultsTool()
        tool.optionsRunner.createNonRowOptionsMenu(optionsContextMenu)
        optionsContextMenu.show(optionsField, Side.BOTTOM, 0.0, 0.0)
    }

    fun onShowParameters() {
        toolPane.parametersTab.isSelected = true
    }

    fun onShowResults() {
        toolPane.selectionModel.select(0)
    }

    fun onCloseResults() {
        val minorTab = toolPane.selectionModel.selectedItem
        if (minorTab != null && minorTab.canClose && minorTab is ResultsTab) {
            minorTab.close()
            if (toolPane.tabs.filterIsInstance<ResultsTab>().isEmpty()) {
                close()
            }
        }
    }

    override fun focusOption() {
        ToolApp.logFocus("HalfTab_Impl focusOption. RequestFocus.requestFocus(optionsField)")
        optionsField.requestFocusWhenSceneSet()
    }

    override fun focusOtherHalf() {
        val other = if (projectTab.left === this) projectTab.right else projectTab.left

        other?.toolPane?.focusResults()
    }

    override fun projectWindow(): ProjectWindow = projectTab.projectTabs.projectWindow

    override fun addAfter(newTool: Tool, run: Boolean, select: Boolean) {
        val tab = projectTab.projectTabs.addAfter(projectTab, newTool, run)
        if (select) {
            tab.isSelected = true
        }
    }

    override fun isSelectedTab(): Boolean = projectTab.isSelected

    override fun selectTab() {
        projectTab.isSelected = true
    }

    override fun isDock(): Boolean = false

}
