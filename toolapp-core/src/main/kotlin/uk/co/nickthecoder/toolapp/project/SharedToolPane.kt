/*
ParaTask Copyright (C) 2017  Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

package uk.co.nickthecoder.toolapp.project

import javafx.event.ActionEvent
import javafx.scene.control.Button
import javafx.scene.control.SingleSelectionModel
import uk.co.nickthecoder.toolapp.Tool
import uk.co.nickthecoder.toolapp.ToolApp

class SharedToolPane(override var tool: Tool) : ToolPane {

    val shared: ToolPane = tool.toolPane!!

    override val tabs: List<MinorTab>
        get() = shared.tabs

    override val selectionModel: SingleSelectionModel<MinorTab>
        get() = shared.selectionModel

    override val halfTab: HalfTab
        get() = shared.halfTab

    override val parametersPane: ParametersPane
        get() = shared.parametersPane

    override val parametersTab: ParametersTab
        get() = shared.parametersTab

    override fun resultsTool(): Tool = shared.resultsTool()

    override fun replaceResults(resultsList: List<Results>, oldResultsList: List<Results>) {
        shared.replaceResults(resultsList, oldResultsList)
    }

    override fun addResults(results: Results): ResultsTab {
        return shared.addResults(results)
    }

    override fun addResults(results: Results, index: Int): ResultsTab {
        return shared.addResults(results, index)
    }

    override fun attached(halfTab: HalfTab) {}

    override fun detaching() {}

    override fun selected() {}

    override fun isAttached() = shared.isAttached()

    override fun nextTab() {
        shared.nextTab()
    }

    override fun prevTab() {
        shared.prevTab()
    }

    override fun selectTab(index: Int) {
        shared.selectTab(index)
    }

    override fun focusHeader() {
        ToolApp.logFocus("SharedToolPane.focusHeader")
        shared.focusHeader()
    }

    override fun focusResults() {
        ToolApp.logFocus("SharedToolPane.focusResults")
        shared.focusResults()
    }

    override var skipFocus: Boolean
        get() = shared.skipFocus
        set(v) {
            shared.skipFocus = v
        }

    override fun currentResults(): Results? {
        return shared.currentResults()
    }

    override fun createAddTabButton(action: (ActionEvent) -> Unit): Button? {
        return null
    }

}