package uk.co.nickthecoder.toolapp.util

import org.junit.Test
import uk.co.nickthecoder.paratask.util.child
import uk.co.nickthecoder.paratask.util.currentDirectory
import uk.co.nickthecoder.toolapp.misc.WrappedFile
import java.io.File

class WrappedFileTest {

    val base = currentDirectory.child("src", "test", "resources", "WrappedFile")

    @Test
    fun file() {

        val file = WrappedFile(File(base, "file"))
        assert(file.isFile())
        assert(!file.isDirectory())
    }

    @Test
    fun directory() {

        val directory = WrappedFile(File(base, "directory"))
        assert(!directory.isFile())
        assert(directory.isDirectory())
    }
}
