ToolApp
========

ToolApp is half way house between a traditional GUI and command line tools.
It contains a set of disparate tools, which can all work together within a single tabbed GUI application.

It includes a file manager, a text editor, a git tool, a terminal, a front end for "grep"...

You could argue that it is a jack of all trades and master of none.
However, the real benefits of ToolApp become apparent when you start customising it.

Every tool has one thing in common, they have "Options". An option is typically a 1 or two character code, which is
used to run a script or a "Task".
Typically a script is just just a single line of code, but could be much longer.
A "Task" is similar to a command line program, but instead of having to remember the command line options,
Task can be prompted in a graphic manner, and their parameters are checked before the task is executed.

For more information, head over to : http://nickthecoder.co.uk/wiki/view/software/ParaTask

Build
-----
There is a dependency on terminalfx, which I found tricky to include within this project.
I am using Debina Buster, which uses OpenJDK version 11 by default, and I like using
Debian's defaults.
However the pre-packaged terminalfx packages use Java version 12.
So, I compiled it from sources :

    git clone https://github.com/rahmanusta/TerminalFX
    cd TerminalFX
    
    # Edit pom.xml, and change the line :
    #    <javafx.version>12</javafx.version>
    # to
    #    <javafx.version>11</javafx.version>
    #
    # At the time of writing, the pom was referencing a repository that has closed down.
    # To fix this, I commented out this section :
    #
    # <!--
    #    <repository>
    #        <id>bintray-jetbrains-pty4j</id>
    #        <name>bintray</name>
    #        <url>https://jetbrains.bintray.com/pty4j</url>
    #    </repository>
    #  -->
    #
    # Then added the following repository :
    #
    # <repository>
    #    <id>jetbrains-intellij-dependencies</id>
    #    <url>https://packages.jetbrains.team/maven/p/ij/intellij-dependencies</url>
    # </repository>
    #
    # And changed the pty4j version to :
    #    <pty4j.version>0.11.4</pty4j.version>
    
    mvn clean install
    cd ..

If you are using version 12 or later, then you could uncomment the terminalfx
repository, and skip the steps above.

If you can't (or don't want to) do either of these, you can compile toolapp without
terminal support by commenting out the line :

    include 'toolapp-terminalfx'

in settings.gradle and also the line :

    compile project(':toolapp-terminalfx')

in build.gradle.
The application should still compile and run, but the terminal will be replaced
by a working, but rather naff alternative.
It lets you run commands from a text field, and shows the results in a seperate
text area. Many shell features won't work, such as command history.


Now we can compile toolapp, using mavenLocal to retrieve terminalfx package.

    git clone https://gitlab.com/nickthecoder/toolapp.git
    cd toolapp
    ./gradlew

To start ToolApp run the "toolapp" script within the build's bin folder.

    ./build/install/toolapp/bin/toolapp

